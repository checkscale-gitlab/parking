package com.kt.mvc.parking.model;

public enum Price {

    MOTORCYCLE(10),
    CAR(15),
    TRUCK(20);

    private int price;

    Price(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;

    }

}
