package com.kt.mvc.parking.model.vehicles;

import java.util.ArrayList;
import java.util.List;

public class HistoryVehicleDAO {

    List<HistoryVehicle> vehicleHistoryList = new ArrayList<>();

    public void addEntry(Vehicle vehicle){
        vehicleHistoryList.add(new HistoryVehicle(vehicle));
    }

    public List<HistoryVehicle> getVehicleHistoryList() {
        return vehicleHistoryList;
    }

    public void reset(){
        vehicleHistoryList = new ArrayList<>();
    }
}
