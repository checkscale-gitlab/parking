package com.kt.mvc.parking.model;

import com.kt.mvc.parking.controller.parkingController.ParkingMVC;
import com.kt.mvc.parking.controller.printer.PrinterMVC;
import com.kt.mvc.parking.model.parkingSpace.ParkingSpace;
import com.kt.mvc.parking.model.parkingSpace.ParkingSpaceDAO;
import com.kt.mvc.parking.model.vehicles.HistoryVehicle;
import com.kt.mvc.parking.model.vehicles.HistoryVehicleDAO;
import com.kt.mvc.parking.model.vehicles.Vehicle;
import com.kt.mvc.parking.model.vehicles.VehicleDAO;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Parking implements ParkingMVC.Model {

    private static Parking instance;

    private ParkingSpaceDAO parkingSpaceDAO;
    private VehicleDAO vehicleDAO;
    private HistoryVehicleDAO historyVehicleDAO;
    private PrinterMVC.Controller printerController;

    private Parking(ParkingSpaceDAO parkingSpaceDAO, VehicleDAO vehicleDAO, HistoryVehicleDAO historyVehicleDAO,
                    PrinterMVC.Controller printerController) {
        this.parkingSpaceDAO = parkingSpaceDAO;
        this.vehicleDAO = vehicleDAO;
        this.historyVehicleDAO = historyVehicleDAO;
        this.printerController = printerController;
    }

    public static Parking create(ParkingSpaceDAO parkingSpaceDAO, VehicleDAO vehicleDAO,
                                              HistoryVehicleDAO historyVehicleDAO,
                                              PrinterMVC.Controller printerController) {
            instance = new Parking(parkingSpaceDAO, vehicleDAO, historyVehicleDAO, printerController);
        return instance;
    }

    public double calculatePrice(int vehicleId) {
        if (vehicleDAO.getAuthorizedVehicleList().stream().anyMatch(p -> p.getVehicleId() == vehicleId)) {
            Vehicle tempVehicle = vehicleDAO.getAuthorizedVehicleList().stream()
                    .filter(p -> p.getVehicleId() == vehicleId)
                    .findFirst()
                    .get();

            double price = (ChronoUnit.SECONDS.between(tempVehicle.getStartTime(), tempVehicle.getEndTime()))
                    / tempVehicle.getHOURS_FACTOR() * tempVehicle.getRate().getPrice();

            return price;
        } else {
            printerController.println("Brak pojazdu w bazie!");
            return 0;
        }
    }

    public double calculatePriceForDay() {

        double sum = 0;
        List<Double> list = new ArrayList<>();

        List<Vehicle> vehicleList = historyVehicleDAO.getVehicleHistoryList().stream()
                .filter(p -> p.getEndTime().isAfter(LocalDateTime.now().minusDays(1)))
                .collect(Collectors.toList());

        vehicleList.forEach(it -> {
            list.add((ChronoUnit.SECONDS.between(it.getStartTime(), it.getEndTime()))
                    / it.getHOURS_FACTOR() * it.getRate().getPrice());
        });
        if (!list.isEmpty()) {
            sum = list.stream().reduce((a, b) -> a + b).get();
        }
        return sum;
    }

    public void parkVehicle(int vehicleId) {

        if (permissionForPark(vehicleId)) {
            ParkingSpace parkingSpace = parkingSpaceDAO.getParkingSpacesList().stream().
                    filter(p -> p.getVehicleId() == -1)
                    .findFirst()
                    .get();
            parkingSpace.setVehicleId(vehicleId);

            Vehicle vehicle = vehicleDAO.getAuthorizedVehicleList().stream()
                    .filter(p -> p.getVehicleId() == vehicleId)
                    .findFirst()
                    .get();
            vehicle.setStartTime(LocalDateTime.now());
        } else {
            printerController.println("Brak pozwolenia na wjazd!");
        }

    }

    public boolean checkFreeSpaces() {

        if (parkingSpaceDAO.getFreeParkingSpaces().stream().filter(p -> p.getVehicleId() == -1).count() >= 1) {

            return true;

        } else if (parkingSpaceDAO.getParkingSpacesList().size() < parkingSpaceDAO.getParkingCapacity()) {

            parkingSpaceDAO.addParkingSpace();
            return true;

        } else {
            return false;
        }

    }

    public boolean permissionForPark(int vehicleId) {

        if (vehicleDAO.getAuthorizedVehicleList().stream()
                .anyMatch(p -> p.getVehicleId() == vehicleId)
                & parkingSpaceDAO.getParkedVehicleIdList().stream().noneMatch(p -> p == vehicleId)
                & checkFreeSpaces()) {

            return true;

        } else {
            return false;
        }

    }

    public void showParkedVehicles() {

        parkingSpaceDAO.getParkedVehicleIdList().forEach(it -> {
            printerController.println("ID miejsca: " + parkingSpaceDAO.getParkingSpacesList().stream()
                    .filter(p -> p.getVehicleId() == it).map(p -> p.getParkingSpaceId()).findFirst());
            printerController.println("Pojazd: " + vehicleDAO.getAuthorizedVehicleList().stream()
                    .filter(p -> p.getVehicleId() == it).findFirst().get());
            printerController.println("Data zaparkowania: " + vehicleDAO.getAuthorizedVehicleList().stream()
                    .filter(p -> p.getVehicleId() == it).findFirst().get().getStartTime());
        });

    }

    public void unparkVehicle(int vehicleId) {
        if (vehicleDAO.getAuthorizedVehicleList().stream().anyMatch(p -> p.getVehicleId() == vehicleId)) {

            Vehicle vehicle = vehicleDAO.getAuthorizedVehicleList().stream()
                    .filter(p -> p.getVehicleId() == vehicleId)
                    .findFirst()
                    .get();
            vehicle.setEndTime(LocalDateTime.now());

            historyVehicleDAO.addEntry(new HistoryVehicle(vehicle));

            ParkingSpace parkingSpace = parkingSpaceDAO.getParkingSpacesList().stream()
                    .filter(p -> p.getVehicleId() == vehicleId)
                    .findFirst()
                    .get();
            parkingSpace.setVehicleId(-1);
        } else {
            printerController.println("Brak pojazdu w bazie!");
        }

    }

    public ParkingSpaceDAO getParkingSpaceDAO() {
        return parkingSpaceDAO;
    }

    public VehicleDAO getVehicleDAO() {
        return vehicleDAO;
    }

    public HistoryVehicleDAO getHistoryVehicleDAO() {
        return historyVehicleDAO;
    }

    public void reset(){
        parkingSpaceDAO.reset();
        vehicleDAO.reset();
        historyVehicleDAO.reset();
    }
}
