package com.kt.mvc.parking.model.parkingSpace;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingSpaceDAO implements ParkingSpaceInterface {


    private List<ParkingSpace> parkingSpacesList = new ArrayList<>();

    private int parkingCapacity;

    public ParkingSpaceDAO() {
        this.parkingCapacity = 10;
    }

    public void addParkingSpace() {

        ParkingSpace parkingSpace = new ParkingSpace();

        if (parkingSpacesList.isEmpty()) {
            parkingSpace.setParkingSpaceId(1);
        } else {
            parkingSpace.setParkingSpaceId(parkingSpacesList.get(parkingSpacesList.size() - 1).getParkingSpaceId() + 1);
        }
        parkingSpacesList.add(parkingSpace);

    }

    public List<Integer> getParkedVehicleIdList() {

        List<Integer> parkedVehicleIdList = parkingSpacesList.stream()
                .filter(p -> p.getVehicleId() != -1)
                .map(p -> p.getVehicleId())
                .collect(Collectors.toList());

        return parkedVehicleIdList;
    }

    public List<ParkingSpace> getFreeParkingSpaces() {

        if (parkingSpacesList.size() < parkingCapacity) {
            int missingPlaces = parkingCapacity - parkingSpacesList.size();
            for (int i = 0; i < missingPlaces; i++) {
                addParkingSpace();
            }
        }
        List<ParkingSpace> freeParkingSpacesIdList = parkingSpacesList.stream()
                .filter(p -> p.getVehicleId() == -1)
                .collect(Collectors.toList());
        return freeParkingSpacesIdList;
    }

    public List<ParkingSpace> getParkingSpacesList() {
        if (parkingSpacesList.size() < parkingCapacity) {
            int missingPlaces = parkingCapacity - parkingSpacesList.size();
            for (int i = 0; i < missingPlaces; i++) {
                addParkingSpace();
            }
        }
        return parkingSpacesList;
    }

    public void increaseParkingCapacity(int newCapacity) {
        if (getParkedVehicleIdList().size() < newCapacity) {
            parkingCapacity = newCapacity;
        }
    }

    public int getParkingCapacity() {
        return parkingCapacity;
    }

    public void reset(){
        parkingSpacesList = new ArrayList<>();
        parkingCapacity = 10;
    }
}



