package com.kt.mvc.parking.model.vehicles;

import com.kt.mvc.parking.model.Price;

import java.time.LocalDateTime;

public abstract class Vehicle  {



    private final double HOURS_FACTOR = 10;

    public abstract int getVehicleId() ;

    public abstract void setVehicleId(int vehicleId);

    public abstract LocalDateTime getStartTime();

    public abstract void setStartTime(LocalDateTime startTime) ;

    public abstract LocalDateTime getEndTime() ;

    public abstract void setEndTime(LocalDateTime endTime) ;

    public double getHOURS_FACTOR() {
        return HOURS_FACTOR;
    }

    public abstract Price getRate();

}
