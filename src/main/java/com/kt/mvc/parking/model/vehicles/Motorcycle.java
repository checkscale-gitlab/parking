package com.kt.mvc.parking.model.vehicles;

import com.kt.mvc.parking.model.Price;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@Getter
@Setter
public class Motorcycle extends Vehicle {
    private int vehicleId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    @Override
    public Price getRate() {
        return Price.MOTORCYCLE;
    }

}
