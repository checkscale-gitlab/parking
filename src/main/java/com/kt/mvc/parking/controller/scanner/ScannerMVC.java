package com.kt.mvc.parking.controller.scanner;

public interface ScannerMVC {
    interface Controller {

        int nextInt();

        int pickOption();
    }
}
