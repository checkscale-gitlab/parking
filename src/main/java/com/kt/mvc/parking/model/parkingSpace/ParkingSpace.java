package com.kt.mvc.parking.model.parkingSpace;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParkingSpace {

    private int parkingSpaceId;
    private int vehicleId = -1;

    @Override
    public String toString() {
        return "ID miejsca: " + parkingSpaceId + ", ID pojazdu: " + vehicleId;
    }
}
