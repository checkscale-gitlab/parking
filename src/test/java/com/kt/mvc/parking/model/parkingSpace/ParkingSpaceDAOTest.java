package com.kt.mvc.parking.model.parkingSpace;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingSpaceDAOTest {

    ParkingSpaceDAO parkingSpaceDAO;

    @BeforeEach
    void setup() {
        parkingSpaceDAO = new ParkingSpaceDAO();
    }

    void parkCars(int howMany) {
        for (int i = 0; i < howMany; i++) {
            parkingSpaceDAO.getParkingSpacesList().get(i).setParkingSpaceId(i);
            parkingSpaceDAO.getParkingSpacesList().get(i).setVehicleId(i);
        }
    }

    @Test
    void shouldReturnCorrectDataWhenOneCarParked() {
        //when
        parkCars(1);
        //then
        assertAll(
                () -> assertEquals(9, parkingSpaceDAO.getFreeParkingSpaces().size()),
                () -> assertEquals(1, parkingSpaceDAO.getParkedVehicleIdList().size())
        );

    }

    @Test
    void shouldReturnCorrectDataWhenTenCarsParked() {
        //when
        parkCars(10);
        //then
        assertAll(
                () -> assertEquals(0, parkingSpaceDAO.getFreeParkingSpaces().size()),
                () -> assertEquals(10, parkingSpaceDAO.getParkedVehicleIdList().size())
        );

    }

    @Test
    void shouldIncreaseParkingSpaceCapacityWhenCapacityIncreased() {
        //when
        parkingSpaceDAO.increaseParkingCapacity(11);
        //then
        assertAll(
                () -> assertEquals(11, parkingSpaceDAO.getFreeParkingSpaces().size()),
                () -> assertEquals(11, parkingSpaceDAO.getParkingSpacesList().size()),
                () -> assertEquals(11, parkingSpaceDAO.getParkingCapacity())
        );
    }
}