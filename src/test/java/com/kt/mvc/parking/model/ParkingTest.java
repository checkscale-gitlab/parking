package com.kt.mvc.parking.model;

import com.kt.mvc.parking.controller.printer.PrinterMVC;
import com.kt.mvc.parking.model.parkingSpace.ParkingSpaceDAO;
import com.kt.mvc.parking.model.vehicles.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


class ParkingTest {

    private ParkingSpaceDAO parkingSpaceDAO;
    private VehicleDAO vehicleDAO ;
    private HistoryVehicleDAO historyVehicleDAO ;
    private PrinterMVC.Controller printerController;
    private Parking parking;
    private Car car;
    private Truck truck;
    private Motorcycle motorcycle;


    @BeforeEach
    void setup() {
        parkingSpaceDAO = new ParkingSpaceDAO();
        vehicleDAO = new VehicleDAO();
        historyVehicleDAO = new HistoryVehicleDAO();
        printerController = mock(PrinterMVC.Controller.class);
        car = new Car();
        truck = new Truck();
        motorcycle = new Motorcycle();
        motorcycle.setVehicleId(20);
        truck.setVehicleId(30);
        car.setVehicleId(10);
        parking = Parking.create(parkingSpaceDAO, vehicleDAO, historyVehicleDAO, printerController);
    }



    @Test
     void shouldCalculatePriceWhenVehicleSelctedById() {
        //given
        car.setStartTime(LocalDateTime.now());
        car.setEndTime(LocalDateTime.now().plusSeconds(20));
        parking.getVehicleDAO().addVehicle(car);
        //then
        assertEquals(30, parking.calculatePrice(1));
    }

    @Test
     void shouldPrintErrorMessagesWhenNotExistingVehicleSelectedForPark() {
        //when
        parking.parkVehicle(999999);
        //then
        verify(printerController, times(1)).println(anyString());
    }

    @Test
    void shouldPrintErrorMessagesWhenNotExistingVehicleSelectedForCalculatePrice() {
        //when
        parking.calculatePrice(9999999);
        //then
        verify(printerController, times(1)).println(anyString());
    }

    @Test
     void shouldCalculateCorrectPriceWhenVehiclesParkOneDay() {
        //given
        car.setStartTime(LocalDateTime.now().minusDays(2));
        motorcycle.setStartTime(LocalDateTime.now().minusMinutes(1));
        truck.setStartTime(LocalDateTime.now().minusMinutes(1));
        car.setEndTime(LocalDateTime.now().minusDays(1));
        motorcycle.setEndTime(LocalDateTime.now());
        truck.setEndTime(LocalDateTime.now());
        parking.getHistoryVehicleDAO().addEntry(car);
        parking.getHistoryVehicleDAO().addEntry(motorcycle);
        parking.getHistoryVehicleDAO().addEntry(truck);
        //when
        parking.calculatePriceForDay();
        //then
        assertEquals(180, parking.calculatePriceForDay());
    }

    @Test
     void shouldUpdateFreeSpacesAndParkedVehicleListsWhenVehiclesParked() {
        //given
        parking.getVehicleDAO().addVehicle(car);
        parking.getVehicleDAO().addVehicle(motorcycle);
        parking.getVehicleDAO().addVehicle(truck);
        //when
        parking.parkVehicle(1);
        parking.parkVehicle(2);
        parking.parkVehicle(3);
        //then
        assertAll(
                () -> assertEquals(3, parking.getParkingSpaceDAO().getParkedVehicleIdList().size()),
                () -> assertEquals(7, parking.getParkingSpaceDAO().getFreeParkingSpaces().size())
        );
    }

    @Test
     void shouldUpdateFreeSpacesAndParkedVehicleListsWhenVehiclesParkedAndUnparked() {
        //given
        parking.getVehicleDAO().addVehicle(car);
        parking.getVehicleDAO().addVehicle(motorcycle);
        parking.getVehicleDAO().addVehicle(truck);
        //when
        parking.parkVehicle(1);
        parking.parkVehicle(2);
        parking.parkVehicle(3);
        parking.unparkVehicle(1);
        //then
        assertAll(
                () -> assertEquals(2, parking.getParkingSpaceDAO().getParkedVehicleIdList().size()),
                () -> assertEquals(8, parking.getParkingSpaceDAO().getFreeParkingSpaces().size())
        );
    }

     @Test
     void shouldReturnFalseWhenParkingIsFull() {
        //given
         parking.getParkingSpaceDAO().increaseParkingCapacity(3);
         parking.getVehicleDAO().addVehicle(car);
         parking.getVehicleDAO().addVehicle(motorcycle);
         parking.getVehicleDAO().addVehicle(truck);
         //when
         parking.parkVehicle(1);
         parking.parkVehicle(2);
         parking.parkVehicle(3);
         //then
         assertFalse(parking.checkFreeSpaces());
     }

     @Test
     void shouldNotHavePermissionWhenCheckNotAddedCar() {
         //when
         assertFalse(parking.permissionForPark(999999));
     }

     @Test
     void shouldShowParkingSpaceInformationWhenCarIsParked() {
        //given
         parking.getVehicleDAO().addVehicle(car);
         parking.parkVehicle(1);
         //when
         parking.showParkedVehicles();
         //then
         verify(printerController, atLeast(3)).println(anyString());
         Mockito.reset(printerController);
     }
}